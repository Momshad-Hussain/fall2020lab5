package movie.importer;

import java.util.ArrayList;

public class RemoveDuplicate extends Processor{

	public RemoveDuplicate(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}

	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> newArr = new ArrayList<String>();
		for (int i = 0; i<input.size(); i++)
		{
			String word = input.get(i);
			if(!newArr.contains(word))
			{
				newArr.add(word);
			}
		}
		
		return newArr;
		
	}

}
