package movie.importer;

import java.io.IOException;
import java.util.ArrayList;

public  class ProcessorTest {

	public static void main(String[] args) throws IOException {
		String srcDir = "C:\\Users\\momsh\\fall2020lab5\\testInput";
		String outputDir = "C:\\Users\\momsh\\fall2020lab5\\testOutput";
		String outputDir2 = "C:\\Users\\momsh\\fall2020lab5\\testRemoveOutput";
		LowercaseProcessor myobj = new LowercaseProcessor(srcDir,outputDir);
		RemoveDuplicate myobj2 = new RemoveDuplicate(outputDir,outputDir2);
		myobj.execute();
		myobj2.execute();
	}
}
